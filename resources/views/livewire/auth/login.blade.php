<div class="login-form login-signin">


    <form class="form"  method="post" action="{{ url('/login') }}">
        @csrf
        <div class="pb-13 pt-lg-0 pt-5">
            <h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Demo Apps</h3>
            <span class="text-muted font-weight-bold font-size-h4">Já tem conta aqui?
									<a href="javascript:;" id="kt_login_signup" class="text-primary font-weight-bolder">Cria Já!</a></span>
        </div>
        <div class="form-group">
            <label class="font-size-h6 font-weight-light text-dark">Email</label>
            <div class="input-group mb-3">

                <input type="email"
                       name="email"
                       value="{{ old('email') }}"
                       placeholder="Email"
                       class="form-control @error('email') is-invalid @enderror">
                <div class="input-group-append">
                    <div class="input-group-text"><span class="fas fa-envelope"></span></div>
                </div>
                @error('email')
                <span class="error invalid-feedback">{{ $message }}</span>
                @enderror
            </div>
        </div>


        <div class="form-group">
            <div class="d-flex justify-content-between mt-n5">
                <label class="font-size-h6 font-weight-light text-dark pt-5">Password</label>
                <a href="javascript:;"
                   class="text-primary font-size-h6 font-weight-light text-hover-primary pt-5"
                   id="kt_login_forgot">Forgot Password ?</a>
            </div>
            <div class="input-group mb-3">
                <input type="password"
                       name="password"
                       placeholder="Password"
                       class="form-control @error('password') is-invalid @enderror">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
                @error('password')
                <span class="error invalid-feedback">{{ $message }}</span>
                @enderror

            </div>
        </div>

        <div class="pb-lg-0 pb-5">

            <div class="row">
                <div class="col-8 pt-3 pb-3">
                    <label class="checkbox checkbox-outline checkbox-success">
                        <input type="checkbox" name="Checkboxes15" checked="checked" id="remember"/>
                        <span></span>
                        &nbsp; Lembrar a minha senha
                    </label>
                </div>

                <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block">Entrar</button>
                </div>

            </div>


        </div>
        <!--end::Action-->
    </form>
    <!--end::Form-->
</div>
