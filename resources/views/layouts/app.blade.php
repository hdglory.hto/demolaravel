<!DOCTYPE html>

<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>

    <meta charset="utf-8"/>
    <title>{{ config('app.name') }}</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link rel="canonical" href="https://keenthemes.com/metronic"/>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>

    <link href="/assets/plugins/custom/fullcalendar/fullcalendar.bundle49d8.css?v=7.2.8"
          rel="stylesheet" type="text/css"/>

    <link href="/assets/plugins/global/plugins.bundle49d8.css?v=7.2.8" rel="stylesheet"
          type="text/css"/>
    <link href="/assets/plugins/custom/prismjs/prismjs.bundle49d8.css?v=7.2.8" rel="stylesheet"
          type="text/css"/>
    <link href="/assets/css/style.bundle49d8.css?v=7.2.8" rel="stylesheet" type="text/css"/>

    <link href="/assets/css/themes/layout/header/base/light49d8.css?v=7.2.8" rel="stylesheet"
          type="text/css"/>
    <link href="/assets/css/themes/layout/header/menu/light49d8.css?v=7.2.8" rel="stylesheet"
          type="text/css"/>
    <link href="/assets/css/themes/layout/brand/dark49d8.css?v=7.2.8" rel="stylesheet"
          type="text/css"/>
    <link href="/assets/css/themes/layout/aside/dark49d8.css?v=7.2.8" rel="stylesheet"
          type="text/css"/>

    @livewireStyles

<body id="kt_body"
      class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">

@livewire('layouts.header-mobile')
<div class="d-flex flex-column flex-root">
    <div class="d-flex flex-row flex-column-fluid page">
        @livewire('layouts.side-bar')

        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">

            @livewire('layouts.hearder-fixed')
            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                @yield('content')
            </div>
            @livewire('layouts.footer-fixed')

        </div>

    </div>
</div>


<script>var KTAppSettings = {
        "breakpoints": {"sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400},
        "colors": {
            "theme": {
                "base": {
                    "white": "#ffffff",
                    "primary": "#3699FF",
                    "secondary": "#E5EAEE",
                    "success": "#1BC5BD",
                    "info": "#8950FC",
                    "warning": "#FFA800",
                    "danger": "#F64E60",
                    "light": "#E4E6EF",
                    "dark": "#181C32"
                },
                "light": {
                    "white": "#ffffff",
                    "primary": "#E1F0FF",
                    "secondary": "#EBEDF3",
                    "success": "#C9F7F5",
                    "info": "#EEE5FF",
                    "warning": "#FFF4DE",
                    "danger": "#FFE2E5",
                    "light": "#F3F6F9",
                    "dark": "#D6D6E0"
                },
                "inverse": {
                    "white": "#ffffff",
                    "primary": "#ffffff",
                    "secondary": "#3F4254",
                    "success": "#ffffff",
                    "info": "#ffffff",
                    "warning": "#ffffff",
                    "danger": "#ffffff",
                    "light": "#464E5F",
                    "dark": "#ffffff"
                }
            },
            "gray": {
                "gray-100": "#F3F6F9",
                "gray-200": "#EBEDF3",
                "gray-300": "#E4E6EF",
                "gray-400": "#D1D3E0",
                "gray-500": "#B5B5C3",
                "gray-600": "#7E8299",
                "gray-700": "#5E6278",
                "gray-800": "#3F4254",
                "gray-900": "#181C32"
            }
        },
        "font-family": "Poppins"
    };</script>
@livewireScripts
<script src="/assets/plugins/global/plugins.bundle49d8.js?v=7.2.8"></script>
<script src="/assets/plugins/custom/prismjs/prismjs.bundle49d8.js?v=7.2.8"></script>
<script src="/assets/js/scripts.bundle49d8.js?v=7.2.8"></script>
<script src="/assets/js/engage_code.js"></script>
<!--end::Global Theme Bundle-->
<!--begin::Page Vendors(used by this page)-->
<script src="/assets/plugins/custom/fullcalendar/fullcalendar.bundle49d8.js?v=7.2.8"></script>
<!--end::Page Vendors-->
<!--begin::Page Scripts(used by this page)-->
<script src="/assets/js/pages/widgets49d8.js?v=7.2.8"></script>
<!--end::Page Scripts-->
</body>
<!--end::Body-->

<!-- Mirrored from preview.keenthemes.com/metronic/demo1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 29 Jul 2021 12:41:43 GMT -->
</html>
