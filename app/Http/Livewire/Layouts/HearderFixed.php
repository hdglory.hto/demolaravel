<?php

namespace App\Http\Livewire\Layouts;

use Livewire\Component;

class HearderFixed extends Component
{
    public function render()
    {
        return view('livewire.layouts.hearder-fixed');
    }
}
