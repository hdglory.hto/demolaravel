<?php

namespace App\Http\Livewire\Layouts;

use Livewire\Component;

class HeaderMobile extends Component
{
    public function render()
    {
        return view('livewire.layouts.header-mobile');
    }
}
