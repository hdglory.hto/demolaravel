<?php

namespace App\Http\Livewire\Layouts;

use Livewire\Component;

class FooterFixed extends Component
{
    public function render()
    {
        return view('livewire.layouts.footer-fixed');
    }
}
